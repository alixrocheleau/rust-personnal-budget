const invoke = window.__TAURI__.invoke

export async function invokeHello(name) {
    return await invoke("hello", {name: name});
}
export async function invokeGetBudgetList() {
    return await invoke("get_budget_list");
}
export async function invokeCreateTag(tag_str) {
    return await invoke("create_tag",{tag_str: tag_str});
}
export async function invokeDeleteTag(index) {
    return await invoke("delete_tag",{index: index});
}
export async function invokeCreateBudget(budget) {
    return await invoke("create_budget",{budget: budget});
}
export async function invokeDeleteBudget(index) {
    return await invoke("delete_budget",{index: index});
}
export async function invokeCreateTransaction(budgetid,is_goal,transac) {
    return await invoke("create_transaction",{budgetid:budgetid,is_goal:is_goal,transac: transac});
}
export async function invokeDeleteTransaction(monthid,budgetid,is_goal,index) {
    return await invoke("delete_transaction",{monthid:monthid,budgetid:budgetid,is_goal:is_goal,index: index});
}


