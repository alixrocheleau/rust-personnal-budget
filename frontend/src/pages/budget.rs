use crate::app::Route;
use crate::models::budget_models::Budget;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;

use yew::prelude::*;
use yew_router::prelude::*;

pub enum Msg {
    LoadBudget(Budget),
}
pub struct BudgetData {
    pub budget: Option<Budget>,
}
#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub id: i32,
}
impl Component for BudgetData {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
         let link = ctx.link().clone();
         let budget_id_prop=ctx.props().id.clone();
                spawn_local(async move {          
                    match get_budget_list().await {
                        Ok(jsvalue) => {     
                        let parsed_data: serde_json::Value =
                            serde_json::from_str(&jsvalue.as_string().unwrap()).unwrap();
                        let budget_list_json: Vec<String> =
                            serde_json::from_value(parsed_data["budgetlist"].clone()).unwrap();
                        let budget_list: Vec<Budget> = budget_list_json
                            .into_iter()
                            .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                            .collect();
                        let index = budget_list
                            .iter()
                            .position(|budgetcheck| {
                                if budgetcheck.id  == budget_id_prop {
                                   true
                                } else {
                                    false
                                }
                            });
                        let budget= budget_list[index.unwrap() as usize].clone();
                        let budgettoload= budget.clone();
                        link.send_message(Msg::LoadBudget(budgettoload.clone()));
                        }
                        Err(e) => {                    
                            log::info!("Error: {:?}", e);
                        }
                    }
                    });
        Self { budget: None }
    }
    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::LoadBudget(budget) => {
                self.budget = Some(budget.clone());
                   log::info!("Loaded budget: {:?}", self.budget);
                true
            }
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        if let Some(current_budget) = &self.budget{
            let goalincome=current_budget.get_goal_income();
            let goalexpanse=current_budget.get_goal_expanse();
            let total_goal=goalincome-goalexpanse;
            let totincome=current_budget.get_income();
            let totexpanse=current_budget.get_expanse();
            let total_real=totincome-totexpanse;
            let total_global=(totincome-totexpanse)+(goalincome-goalexpanse);
        html! {
        <>
          <div class="container">
              <h1>{current_budget.name.clone()}</h1>
              <table class="table table-adapt table-striped">
              <thead>
              <tr>
                  <th>{"Month"}</th>
                  <th>{"Goal"}</th>
                  <th>{"Real"}</th>
                  <th>{"Balance"}</th>
                  <th>{"Action"}</th>
              </tr>
              </thead>
              <tbody>
                {
                    for current_budget.months.iter().map(|month|{
                    let navigator = ctx.link().navigator().unwrap();
                    let bid=current_budget.id.clone();
                    let mid=month.id.clone();
                    let onclicked = Callback::from(move |_| navigator.push(&Route::MonthData {budget_id:bid , id:mid}));
                        let goalincome=month.get_total_goal_income();
                        let goalexpanse=month.get_total_goal_expanse();
                        let totincome=month.get_total_income();
                        let totexpanse=month.get_total_expanse();
                        html!{
                            <tr class="table-link">
                                <td onclick={&onclicked}>{month.name.clone()}</td>
                                <td onclick={&onclicked}>{((goalincome-goalexpanse)*100.00).round()/100.00}</td>
                                <td onclick={&onclicked}>{((totincome-totexpanse)*100.00).round()/100.00}</td>
                                <td onclick={&onclicked}>{(((totincome-totexpanse)+(goalincome-goalexpanse))*100.00).round()/100.00}</td>
                                <td >
                                <div class="">
                                  <Link<Route> classes={"btn btn-primary btn-sm m-1"} to={Route::MonthData {budget_id:current_budget.id , id:month.id}}>
                                  {"View"}
                                  </Link<Route>>
                                  // <button class="btn btn-danger btn-sm m-1">{"Reset"}</button>
                                </div>
                              </td>
                            </tr>
                        }
                    })
                    
                }
                <tr>
                        <td>{"Total"}</td>
                        <td>{(total_goal*100.00).round()/100.00}</td>
                        <td>{(total_real*100.00).round()/100.00}</td>
                        <td>{(total_global*100.00).round()/100.00}</td>
                        <td>{"=(^.^)="}</td>
                    </tr>
              </tbody>
              </table>
              // <div class="p-2 m-2">
              // <h2>{"Add transaction"}</h2>
              //     <div class="d-flex">
              //     <form>
              //         <div class="form-group">
              //         <label for="name">{"Amount:"}</label>
              //         <input type="number" class="form-control" min="0.00" step="0.01" id="amount" placeholder="100.00"/>
              //         </div>
              //         <button type="button" class="btn btn-primary">{"Create"}</button>
              //     </form>
              //     </div>
              // </div>
          </div>
        </>
        }
    }else{
        html!{
            <div>
            {"Loading budget or not found"}
            </div>
        }
    }
    }
}

#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeGetBudgetList, catch)]
    pub async fn get_budget_list() -> Result<JsValue, JsValue>;
}