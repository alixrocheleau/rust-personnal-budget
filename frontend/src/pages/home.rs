use web_sys::HtmlInputElement;
use serde_json::to_string;
use crate::app::Route;
use crate::models::budget_models::Budget;


use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;

use yew::prelude::*;
use yew_router::prelude::*;

pub enum Msg {
    LoadBudgets(Vec<Budget>),
    CreateModel(),
    AddModelSelf(Budget),
    DeleteBudget(usize),
    RemoveBudget(String),
    UpdateName(String),
}

pub struct Home {
    budgets: Vec<Budget>,
    budget_new_name:String,
}
impl Component for Home {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        spawn_local(async move {          
            match get_budget_list().await {
                Ok(jsvalue) => {     
                let parsed_data: serde_json::Value =
                    serde_json::from_str(&jsvalue.as_string().unwrap()).unwrap();
                let budget_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["budgetlist"].clone()).unwrap();
                let budget_list: Vec<Budget> = budget_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                link.send_message(Msg::LoadBudgets(budget_list.clone()));
                }
                Err(e) => {                    
                    log::info!("Error: {:?}", e);
                }
            }
        });
        Self { budgets: vec![],budget_new_name: "".to_string() }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::LoadBudgets(budget_list) => {
                self.budgets = budget_list.clone();
                   log::info!("Loaded budgets: {:?}", self.budgets);
                true
            }
            Msg::RemoveBudget(id)=> {
                log::info!("Removing budget from list {:?}", id);
                let index=id.parse::<usize>().unwrap();
                self.budgets.remove(index); 
                true
            }
            Msg::DeleteBudget(id) => {
                let link = ctx.link().clone();
                let tag_str = to_string(&id).unwrap();
                let index_id=tag_str.clone();
                log::info!("budget id: {:?}", tag_str);
                spawn_local(async move {          
                    match delete_budget(tag_str).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());                             
                             link.send_message(Msg::RemoveBudget(index_id));
                        }
                        Err(e) => {
                            log::info!("Error deleting budget: {:?}", e);
                        }
                    }
                });
                true
            }
            Msg::AddModelSelf(budgetmodel) => {
                self.budgets.push(budgetmodel);
                true
            }
            Msg::UpdateName(s) => {
                self.budget_new_name = s.to_string();
                true
            }
            Msg::CreateModel() => {
                let link = ctx.link().clone();
                let new_id=self.budgets.iter().map(|budget| budget.id).max().unwrap_or(0) + 1;
                let mut new_budget = Budget {
                    id:new_id,
                    name: self.budget_new_name.clone(),
                    months: vec![]
                };
                new_budget.init();
                let budgetstring = to_string(&new_budget).unwrap();
                let clonedbudget=new_budget.clone();
                log::info!("budget string: {:?}", budgetstring);
                spawn_local(async move {          
                    match create_budget(budgetstring).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());
                            link.send_message(Msg::AddModelSelf(clonedbudget.clone()));
                        }
                        Err(e) => {
                            log::info!("Error creating budget: {:?}", e);
                        }
                    }
                });
                true
            }
        }
        
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
        <>
          <div class="container">
              <h1>{"Budgets"}</h1>
              <table class="table table-adapt table-striped">
              <thead>
              <tr>
                  <th>{"Name"}</th>
                  <th>{"Goal"}</th>
                  <th>{"Real"}</th>
                  <th>{"Balance"}</th>
                  <th>{"Action"}</th>
              </tr>
              </thead>
              <tbody>
                {
                    for self.budgets.iter().enumerate().map(|(indext, budget_item)|{
                    let index=budget_item.id.clone();
                    let t = indext.clone();
                    let navigator = ctx.link().navigator().unwrap();
                    let onclicked = Callback::from(move |_| navigator.push(&Route::BudgetData {id:index}));
                    let goalincome=budget_item.get_goal_income();
                    let goalexpanse=budget_item.get_goal_expanse();
                    let totincome=budget_item.get_income();
                    let totexpanse=budget_item.get_expanse();
                        html!{
                            <tr class="table-link">
                                <td onclick={&onclicked}>{budget_item.name.clone()}</td>
                                <td onclick={&onclicked}>{((goalincome-goalexpanse)*100.00).round()/100.00}</td>
                                <td onclick={&onclicked}>{((totincome-totexpanse)*100.00).round()/100.00}</td>
                                <td onclick={&onclicked}>{(((totincome-totexpanse)+(goalincome-goalexpanse))*100.00).round()/100.00}</td>
                                <td >
                                <div class="">
                                  <Link<Route> classes={"btn btn-primary btn-sm m-1"} 
                                      to={Route::BudgetData 
                                        {id:index}
                                    }>
                                  {"View"}
                                  </Link<Route>>
                                  <button onclick={
                                        ctx.link().callback( move |_e: MouseEvent|{
                                          Msg::DeleteBudget(t.clone())
                                       })} class="btn btn-danger btn-sm m-1">{"Delete"}</button>
                                </div>
                              </td>
                            </tr>
                        }
                    })
                }
              </tbody>
              </table>
              <div class="p-2 m-2">
              <h2>{"Create new budget"}</h2>
                  <div class="d-flex">
                  <form>
                      <div class="form-group">
                      <label for="name">{"Name:"}</label>
                      <input onchange={
                        ctx.link().callback(move |e:Event|{
                           let t =  e.target().unwrap()
                           .dyn_into::<HtmlInputElement>().unwrap()
                           .value();
                            Msg::UpdateName(t)
                        })}
                       type="text" class="form-control" id="name" 
                       value={self.budget_new_name.clone()} placeholder="ex: Budget2022"/>
                      </div>
                      <button onclick={
                        ctx.link().callback(move |_e: MouseEvent|{
                              Msg::CreateModel()})} 
                               type="button" class="btn btn-primary">{"Create"}</button>
                  </form>
                  </div>
              </div>
          </div>
        </>
        }
    }
}

#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeHello, catch)]
    pub async fn hello(name: String) -> Result<JsValue, JsValue>;
}

#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeGetBudgetList, catch)]
    pub async fn get_budget_list() -> Result<JsValue, JsValue>;
}

#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeCreateBudget, catch)]
    pub async fn create_budget(budget: String) -> Result<JsValue, JsValue>;
}
#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeDeleteBudget, catch)]
    pub async fn delete_budget(index: String) -> Result<JsValue, JsValue>;
}