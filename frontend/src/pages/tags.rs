use wasm_bindgen_futures::spawn_local;
use wasm_bindgen::JsValue;
use serde_json::to_string;
use crate::comps::color_selector::ColorSelector;
use crate::models::budget_models::Tag;
use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

pub enum Msg {
    LoadTags(Vec<Tag>),
    UpdateModel(Tag),
    UpdateColor(String),
    UpdateName(String),
    CreateModel(),
    AddModelSelf(Tag),
    DeleteTag(usize),
    RemoveTag(String),
}

pub struct Tags {
    tag_list: Vec<Tag>,
    tag_model: Tag,
}
impl Component for Tags {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        spawn_local(async move {          
            match get_budget_list().await {
                Ok(jsvalue) => {
                    let parsed_data: serde_json::Value =
                    serde_json::from_str(&jsvalue.as_string().unwrap()).unwrap();
                let tag_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["taglist"].clone()).unwrap();
                let tag_list: Vec<Tag> = tag_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                   // log::info!("tags: {:?}", tag_list);
                link.send_message(Msg::LoadTags(tag_list.clone()));
                 }
                Err(e) => {
                   log::info!("Error: {:?}", e);
                }
            }
        });
        Self {
            tag_list: vec![],
            tag_model: Tag {
                name: "Demo".to_string(),
                color: "bg-primary".to_string(),
            },
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::RemoveTag(ind)=> {
                   log::info!("Removing tag 3 from list {:?}", ind);
                let index=ind.parse::<usize>().unwrap();
                self.tag_list.remove(index);
                true
            }
            Msg::LoadTags(tag_list) => {
                self.tag_list = tag_list.clone();
                   log::info!("Loaded tags: {:?}", self.tag_list);
                true
            }
            Msg::UpdateModel(tag_model) => {
                self.tag_model = tag_model.clone();
                true
            }
            Msg::AddModelSelf(tag_model) => {
                self.tag_list.push(tag_model);
                true
            }
            Msg::CreateModel() => {
                let link = ctx.link().clone();
                let new_tag_model=self.tag_model.clone();
                let tag = Tag {
                    name: "Demo".to_string(),
                    color: "bg-primary".to_string(),
                };
                self.tag_model = tag.clone();
                let tag_str = to_string(&new_tag_model).unwrap();
                log::info!("tag string: {:?}", tag_str);
                spawn_local(async move {          
                    match create_tag(tag_str).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());
                        link.send_message(Msg::AddModelSelf(new_tag_model.clone()));
                        }
                        Err(e) => {
                            log::info!("Error creating tag: {:?}", e);
                        }
                    }
                });
                true
            }
            Msg::UpdateColor(s) => {
                self.tag_model.color = s.to_string();
                true
            }
            Msg::UpdateName(s) => {
                self.tag_model.name = s.to_string();
                true
            }
            Msg::DeleteTag(index) => {
                let link = ctx.link().clone();
                let tag_str = to_string(&index).unwrap();
                let indextag=tag_str.clone();
                log::info!("tag index: {:?}", tag_str);
                spawn_local(async move {          
                    match delete_tag(tag_str).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());                             
                             link.send_message(Msg::RemoveTag(indextag));
                        }
                        Err(e) => {
                            log::info!("Error deleting tag: {:?}", e);
                        }
                    }
                });
                true
            }
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        let current_tags = &self.tag_list;
        let tag = Tag {
            name: "Demo".to_string(),
            color: "bg-primary".to_string(),
        };

        html! {
          <>
          <div class="container">
          <h1>{"Tags Table"}</h1>
          <table class="table table-striped">
          <thead>
          <tr>
              <th>{"Name"}</th>
              <th>{"Color"}</th>
              <th>{"Action"}</th>
          </tr>
          </thead>
          <tbody>
          {
            for current_tags.iter().enumerate().map(|(index,item)|{
                let t = index.clone();
                html!{
                  <tr>
                       <td>{item.name.clone()}</td>
                  <td><span class={classes!("badge","rounded-pill",item.color.clone())} >{item.name.clone()}</span></td>
                  <td>
                      // <button type="button" class="btn btn-primary btn-sm m-1" data-toggle="modal" data-target="#tagModal">{"Edit"}</button>
                      <button onclick={
                        ctx.link().callback( move |_e: MouseEvent|{
                              Msg::DeleteTag(t.clone())
                           })} class="btn btn-danger btn-sm m-1">{"Delete"}</button>
                  </td>
                  </tr>
                }
            })
          }
          </tbody>
          </table>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tagModal">
              {"New Tag"}
          </button>
          </div>
            <div class="modal fade" id="tagModal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-top10" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="tagModalLabel">{"Tag Edit"}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">{"x"}</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <ColorSelector
                  current_name={self.tag_model.name.clone()}
                  current_color={self.tag_model.color.clone()}
                  on_color_clicked={ctx.link().callback(|msg| {msg})}
                  />
                      <label for="name">{"Name:"}</label>
                      <input onchange={
                        ctx.link().callback(move |e:Event|{
                           let t =  e.target().unwrap()
                           .dyn_into::<HtmlInputElement>().unwrap()
                           .value();
                            Msg::UpdateName(t)
                        })}
                       type="text" class="form-control" id="name" value={self.tag_model.name.clone()} placeholder="ex: Groceries"/>
                   </div>
                  <div class="modal-footer">
                    <button type="button" onclick={
                        ctx.link().callback( move |_e: MouseEvent|{
                              Msg::UpdateModel(tag.clone())
                           })} class="btn btn-secondary" data-dismiss="modal">{"Close"}</button>
                    <button onclick={
                        ctx.link().callback(move |_e: MouseEvent|{
                              Msg::CreateModel()})}  type="button" class="btn btn-primary" data-dismiss="modal">{"Save changes"}</button>
                  </div>
                </div>
              </div>
            </div>
          </>
        }
    }
}
impl Tags {}


#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeGetBudgetList, catch)]
    pub async fn get_budget_list() -> Result<JsValue, JsValue>;
}

#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeCreateTag, catch)]
    pub async fn create_tag(tag_str: String) -> Result<JsValue, JsValue>;
}
#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeDeleteTag, catch)]
    pub async fn delete_tag(index: String) -> Result<JsValue, JsValue>;
}
