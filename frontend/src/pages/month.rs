use serde_json::to_string;
use crate::models::budget_models::Budget;
use crate::models::budget_models::Tag;
use crate::models::budget_models::Transaction;
use crate::models::budget_models::TransactionType;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;
use web_sys::HtmlInputElement;
use std::collections::HashMap;
use yew::prelude::*;

pub enum Msg {
    NavItemClick(i32),
    LoadTags(Vec<Tag>),
    LoadBudgets(Vec<Budget>),
    UpdateAmount(String),
    UpdateDescription(String),
    CreateTransactionGoal(),
    CreateTransaction(),
    UpdateTransactionType(TransactionType),
    DeleteTransactionGoal(usize),
    DeleteTransaction(usize),
    SelectTag(String),
    UpdateDatetime(String),
    Reload(),
}

pub struct MonthData {
    tag_list: Vec<Tag>,
    budgets: Vec<Budget>,
    nav_id: i32,
    new_description:String,
    new_date:String,
    new_amount:f64,
    selected_tag_name:String,
    new_transac_type:TransactionType,
}
#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub budget_id: i32,
    pub id: i32,
}
struct TagGroup {
    pub tag: Tag,
    pub goal: f64,
    pub real: f64,
}
impl Component for MonthData {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        spawn_local(async move {          
            match get_budget_list().await {
                Ok(jsvalue) => {     
                let parsed_data: serde_json::Value =
                    serde_json::from_str(&jsvalue.as_string().unwrap()).unwrap();
                let budget_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["budgetlist"].clone()).unwrap();
                let budget_list: Vec<Budget> = budget_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                let tag_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["taglist"].clone()).unwrap();
                let tag_list: Vec<Tag> = tag_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                link.send_message(Msg::LoadBudgets(budget_list.clone()));
                link.send_message(Msg::LoadTags(tag_list.clone()));
                }
                Err(e) => {                    
                    log::info!("Error: {:?}", e);
                }
            }
        });
        Self {
            tag_list: vec![],
            budgets: vec![],
            nav_id: 1,
            new_description:"New transaction".to_string(),
            new_amount:0.00,
            new_transac_type:TransactionType::Expense,
            selected_tag_name:0.to_string(),
            new_date:"0000-00-00".to_string()
        }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Reload() => {
                let link = ctx.link().clone();
        spawn_local(async move {          
            match get_budget_list().await {
                Ok(jsvalue) => {     
                let parsed_data: serde_json::Value =
                    serde_json::from_str(&jsvalue.as_string().unwrap()).unwrap();
                let budget_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["budgetlist"].clone()).unwrap();
                let budget_list: Vec<Budget> = budget_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                let tag_list_json: Vec<String> =
                    serde_json::from_value(parsed_data["taglist"].clone()).unwrap();
                let tag_list: Vec<Tag> = tag_list_json
                    .into_iter()
                    .map(|tag_str| serde_json::from_str(&tag_str).unwrap())
                    .collect();
                link.send_message(Msg::LoadBudgets(budget_list.clone()));
                link.send_message(Msg::LoadTags(tag_list.clone()));
                }
                Err(e) => {                    
                    log::info!("Error: {:?}", e);
                }
            }
        });
                true
            }
            Msg::LoadBudgets(budget_list) => {
                self.budgets = budget_list.clone();
                   log::info!("Loaded budgets: {:?}", self.budgets);
                true
            }
            Msg::LoadTags(tag_list) => {
                self.tag_list = tag_list.clone();
                   log::info!("Loaded tags: {:?}", self.tag_list);
                true
            }
            Msg::NavItemClick(id) => {
                self.nav_id = id;
                true
            },
            Msg::UpdateAmount(amount)=>{
               if let Ok(parsed_amount) = amount.parse::<f64>() {
                  self.new_amount = parsed_amount;                    
                } else {
                    self.new_amount =0.0;                    
                }
                true
            }
            Msg::UpdateDescription(descrip)=>{
                self.new_description=descrip.clone();
                true
            }            
            Msg::DeleteTransaction(id)=>{
                let link = ctx.link().clone();
                let id_str = to_string(&id).unwrap();
                log::info!("transac id: {:?}", id_str);
                log::info!("bd id: {:?}", (ctx.props().budget_id+1).to_string().clone());
                let budid=to_string(&(ctx.props().budget_id + 1)).unwrap();
                let monthid=to_string(&(ctx.props().id)).unwrap();
                spawn_local(async move {          
                    match delete_transaction(monthid,budid,false.to_string(),id_str).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());  
                            link.send_message(Msg::Reload());
                        }
                        Err(e) => {
                            log::info!("Error deleting transaction: {:?}", e);
                        }
                    }
                });
                true
            }  
            Msg::DeleteTransactionGoal(id)=>{
                let link = ctx.link().clone();
                let id_str = to_string(&id).unwrap();
                log::info!("transac index: {:?}", id_str);
                log::info!("budget id: {:?}", (ctx.props().budget_id+1).to_string().clone());
                let budid=to_string(&(ctx.props().budget_id + 1)).unwrap();
                let monthid=to_string(&(ctx.props().id)).unwrap();
                spawn_local(async move {          
                    match delete_transaction(monthid,budid,true.to_string(),id_str).await {
                         Ok(jsvalue) => {
                             log::info!("Success: {:?}",jsvalue.as_string().unwrap());  
                            link.send_message(Msg::Reload());
                        }
                        Err(e) => {
                            log::info!("Error deleting transaction: {:?}", e);
                        }
                    }
                });
                true
            }
            Msg::CreateTransaction() => {
                let tag= self.tag_list.iter().filter(|otag| otag.color.clone()==self.selected_tag_name.clone()).collect::<Vec<_>>().first().unwrap().clone();
                let link = ctx.link().clone();
                //budgetid: String,is_goal:bool,transac:String
                let new_transc=Transaction{
                      transaction_type: self.new_transac_type.clone(),
                         amount: self.new_amount.clone(),
                         description: self.new_description.clone(),
                         date: self.new_date.clone(),
                         tags: vec![tag.clone()],
                                    };
                let tag_str = to_string(&new_transc).unwrap();
                log::info!("bd id: {:?}", (ctx.props().budget_id+1).to_string().clone());
                log::info!("bool: {:?}", true);
                log::info!("new transac string: {:?}", tag_str);
                let budid=to_string(&(ctx.props().budget_id + 1)).unwrap();
                spawn_local(async move {          
                    match create_transaction(budid,false.to_string(),tag_str.clone()).await {
                         Ok(jsvalue) => {
                            log::info!("Success: {:?}",jsvalue.as_string().unwrap());
                            link.send_message(Msg::Reload());
                        }
                        Err(e) => {
                            log::info!("Error creating transaction: {:?}", e);
                        }
                    }
                });
                
                true
            }
            Msg::CreateTransactionGoal() => {
                let tag= self.tag_list.iter().filter(|otag| otag.color.clone()==self.selected_tag_name.clone()).collect::<Vec<_>>().first().unwrap().clone();
                let link = ctx.link().clone();
                //budgetid: String,is_goal:bool,transac:String
                let new_transc=Transaction{
                      transaction_type: self.new_transac_type.clone(),
                         amount: self.new_amount.clone(),
                         description: self.new_description.clone(),
                         date: self.new_date.clone(),
                         tags: vec![tag.clone()],
                };
                let tag_str = to_string(&new_transc).unwrap();
                log::info!("bd id: {:?}", (ctx.props().budget_id+1).to_string().clone());
                log::info!("bool: {:?}", true);
                log::info!("new transac string: {:?}", tag_str);
                let budid=to_string(&(ctx.props().budget_id + 1)).unwrap();
                spawn_local(async move {          
                    match create_transaction(budid,true.to_string(),tag_str.clone()).await {
                         Ok(jsvalue) => {
                            log::info!("Success: {:?}",jsvalue.as_string().unwrap());
                            link.send_message(Msg::Reload());
                        }
                        Err(e) => {
                            log::info!("Error creating transaction: {:?}", e);
                        }
                    }
                });
                
                true
            }
            Msg::UpdateTransactionType(new_type)=>{
                self.new_transac_type=new_type;
                true
            }
            Msg::UpdateDatetime(time)=>{
                self.new_date=time;
                true
            }
            Msg::SelectTag(colortext) => {
                log::info!("test: {:?}", colortext);
                self.selected_tag_name = colortext;
                true
            }
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        
        if self.budgets.len() as i32 > 0 {
        let current_budget = &self.budgets[ctx.props().budget_id as usize];
        let current_month = &current_budget.months[ctx.props().id as usize];
        let current_tags = &self.tag_list;
        let mut transac_vec: &Vec<Transaction> = &vec![];
        let mut tag_grouping: Vec<TagGroup> = vec![];

        let transaction_type = self.new_transac_type.clone();
        let on_change = ctx.link().callback(|new_type| {
            Msg::UpdateTransactionType(new_type)
        });
        let mut tags_goal = HashMap::new();
        let mut tags_goal_expense = HashMap::new();
        let mut tags_real = HashMap::new();
        let mut tags_real_expense = HashMap::new();
        if self.nav_id == 1 {
            transac_vec = &current_month.goal_transactions;
        } else if self.nav_id == 2 {
            transac_vec = &current_month.transactions;
        } else {
            for transac in current_month.goal_transactions.iter() {
                let tagname = transac.tags[0].name.clone();
                let mut newamount = 0.0;
                match &transac.transaction_type {
                    TransactionType::Income => {
                        match tags_goal.get(&tagname) {
                            Some(&number) => newamount = newamount + number,
                            _ => println!("Don't have"),
                        }
                        tags_goal.insert(tagname, transac.amount + newamount);
                    }
                    TransactionType::Expense => {
                        match tags_goal_expense.get(&tagname) {
                            Some(&number) => newamount = newamount + number,
                            _ => println!("Don't have"),
                        }
                        tags_goal_expense.insert(tagname, transac.amount + newamount);
                    }
                }
            }
            for transac in current_month.transactions.iter() {
                let tagname = transac.tags[0].name.clone();
                let mut newamount = 0.0;
                match &transac.transaction_type {
                    TransactionType::Income => {
                        match tags_real.get(&tagname) {
                            Some(&number) => newamount = newamount + number,
                            _ => println!("Don't have"),
                        }
                        tags_real.insert(tagname, transac.amount + newamount);
                    }
                    TransactionType::Expense => {
                        match tags_real_expense.get(&tagname) {
                            Some(&number) => newamount = newamount + number,
                            _ => println!("Don't have"),
                        }
                        tags_real_expense.insert(tagname, transac.amount + newamount);
                    }
                }
            }
            for item in current_tags.iter() {
                let mut new_group = TagGroup {
                    tag: item.clone(),
                    goal: 0.0,
                    real: 0.0,
                };
                new_group.goal = match tags_goal.get(&item.name) {
                    Some(&number) => number,
                    _ => 0.0,
                };
                new_group.goal -= match tags_goal_expense.get(&item.name) {
                    Some(&number) => number,
                    _ => 0.0,
                };
                new_group.real = match tags_real.get(&item.name) {
                    Some(&number) => number,
                    _ => 0.0,
                };
                new_group.real -= match tags_real_expense.get(&item.name) {
                    Some(&number) => number,
                    _ => 0.0,
                };
                tag_grouping.push(new_group);
            }
        }
        html! {
        <>
          <div class="container">
              <h1>{current_month.name.clone()}</h1>
                <ul class="nav nav-pills nav-fill align-middle align-items-center">
                    <span class="align-middle">
                    {"Display : "}
                    </span>
                  <li class="nav-item">
                    <a class={classes!("nav-link",if self.nav_id==1{"active"}else{""})} onclick={
                        ctx.link().callback(move |_e: MouseEvent|{
                              Msg::NavItemClick(1)
                           })}
                        >{"Goal"}</a>
                  </li>
                  <li class="nav-item">
                  <a class={classes!("nav-link",if self.nav_id==2{"active"}else{""})} onclick={
                        ctx.link().callback(move |_e: MouseEvent|{
                              Msg::NavItemClick(2)
                           })}
                        >{"Real"}</a>
                  </li>
                  <li class="nav-item">
                  <a class={classes!("nav-link",if self.nav_id==3{"active"}else{""})} onclick={
                        ctx.link().callback(move |_e: MouseEvent|{
                              Msg::NavItemClick(3)
                           })}
                        >{"By Tag"}</a>
                  </li>
                </ul>
                <br/>
                //Table for transactions
                {if self.nav_id!=3{
                    html!{
                <table class="table  table-striped">
                  <thead>
                  <tr>
                      <th>{"Date"}</th>
                      <th style="width:15%;">{"Tags"}</th>
                      <th class="text-end">{"Income"}</th>
                      <th class="text-end">{"Expense"}</th>
                      <th>{"Action"}</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                        for transac_vec.iter().enumerate().map(|(index,transac)|{

                            let transc_index=index.clone();
                            let onclick= match self.nav_id.clone(){
                                1=> {ctx.link().callback( move |_e: MouseEvent|{
                                          Msg::DeleteTransactionGoal(transc_index.clone())
                                       })},
                                2=> {ctx.link().callback( move |_e: MouseEvent|{
                                          Msg::DeleteTransaction(transc_index.clone())
                                       })},
                                _ => {ctx.link().callback( move |_e: MouseEvent|{
                                          Msg::Reload()
                                       })}
                            };
                            let mut income= 0.0;
                            let mut expense=0.0;
                            match &transac.transaction_type {
                                TransactionType::Income=>income=transac.amount,
                                TransactionType::Expense=>expense=transac.amount
                            }
                            html!{
                                <>
                                <tr>
                                    <td>{transac.date.clone().split_off(8)}</td>
                                    <td>
                                    {
                                        for transac.tags.iter().map(|tag|{
                                            html!{
                                                <span class={classes!("badge","rounded-pill",tag.color.clone())} >{tag.name.clone()}</span>
                                            }
                                        })

                                    }</td>
                                    <td class="text-end">{income}</td>
                                    <td class="text-end">{expense}</td>
                                    <td >
                                      // <button class="btn btn-primary btn-sm m-1">{"Edit"}</button>
                                      <button onclick={onclick} class="btn btn-danger btn-sm m-1">{"Delete"}</button>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan=5>
                                    <div class="w-100 d-flex">
                                    <span class="p-2 ml-2">{transac.description.clone()}</span>
                                    </div>
                                    </td>
                                </tr>

                                </>
                            }
                        })
                    }
                  </tbody>
                </table>}
                //Table for tag grouping
                }else{
                    let mut total_goal=0.0;
                    let mut total_real=0.0;
                    let mut total_diff=0.0;
                    for group in tag_grouping.iter(){
                        total_goal=total_goal+group.goal;
                        total_real=total_real+group.real;
                        total_diff=total_diff+ (group.real-group.goal);
                    }

                html!{
                <table class="table  table-striped">
                  <thead>
                  <tr>
                      <th style="width:15%;">{"Tag"}</th>
                      <th class="text-end">{"Goal"}</th>
                      <th class="text-end">{"Real"}</th>
                      <th class="text-end">{"Diff"}</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                    for tag_grouping.iter().map(|group|{
                        html!{
                        <tr>
                            <td>
                            <span class={classes!("badge","rounded-pill",group.tag.color.clone())} >{group.tag.name.clone()}</span>
                            </td>
                            <td class="text-end">{(group.goal*100.00).round()/100.00}</td>
                            <td class="text-end">{(group.real*100.00).round()/100.00}</td>
                            <td class="text-end">{((group.real-group.goal)*100.00).round()/100.00}</td>
                        </tr>
                        }
                    })
                    }
                    <tr>
                        <td>{"Total"}</td>
                        <td class="text-end">{(total_goal*100.00).round()/100.00}</td>
                        <td class="text-end">{(total_real*100.00).round()/100.00}</td>
                        <td class="text-end">{(total_diff*100.00).round()/100.00}</td>
                    </tr>
                  </tbody>
                </table>
                }
                }}
             { if self.nav_id==1{
                html!{
                    <>
                      <div class="p-2 m-2">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#goalModal">
                          {"Add goal"}
                      </button>
                        <div class="modal fade" id="goalModal" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-top10" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="goalModalLabel">{"New Goal"}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">{"x"}</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                   <div class="d-flex">
                                      <form>
                                          <div class="form-group">
                                          <label for="name">{"Amount:"}</label>
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateAmount(t)
                                            })}
                                            type="number" class="form-control" min="0.00" step="0.01" id="amount" value={self.new_amount.to_string()} placeholder="100.00"/>
                                          </div>
                                          <div class="form-group">
                                          <label for="desc">{"Description:"}</label>
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateDescription(t)
                                            })}
                                           type="text" class="form-control" id="name" value={self.new_description.clone()} placeholder="ex: IGA purchases"/>
                                           </div>
                                           <div class="form-group">
                                           <RadioGroup
                                                selected_type={transaction_type}
                                                on_change={on_change}
                                            />
                                           </div>
                                           <div class="form-group">
                                          <label for="tagselect">{"Choose tag:"}</label>
                                           {
                                            for self.tag_list.iter().map(|mytag|{
                                                let tagclone= mytag.clone();
                                                html!{
                                                    <>
                                                        <button type="button" onclick={
                                                        ctx.link().callback( move |_e: MouseEvent|{
                                                              Msg::SelectTag(tagclone.color.clone())
                                                           })} class={classes!("btn","btn-secondary",tagclone.color.clone(),if tagclone.color.to_string()==self.selected_tag_name.clone().to_string() {""}else{"unselected-tag "})} >
                                                        {tagclone.name.clone()}
                                                        </button>
                                                    </>
                                                }
                                            })
                                           }                                           
                                          </div>
                                          <div class="form-group">
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateDatetime(t)
                                            })}
                                           type="date" class="form-control" id="date" value={self.new_date.clone()} placeholder="2023-05-20"/>
                                           </div>
                                      </form>
                                      </div>                      
                               </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{"Close"}</button>
                                <button onclick={
                                    ctx.link().callback(move |_e: MouseEvent|{
                                          Msg::CreateTransactionGoal()})}  type="button" class="btn btn-primary" data-dismiss="modal">{"Save changes"}</button>
                              </div>
                            </div>
                          </div>
                        </div>                        
                      </div>
                      </>
                  }
              }else if self.nav_id==2{
                   html!{
                    <>
                      <div class="p-2 m-2">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#goalModal">
                          {"Add transaction"}
                      </button>
                        <div class="modal fade" id="goalModal" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-top10" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="goalModalLabel">{"New Goal"}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">{"x"}</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                   <div class="d-flex">
                                      <form>
                                          <div class="form-group">
                                          <label for="name">{"Amount:"}</label>
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateAmount(t)
                                            })}
                                            type="number" class="form-control" min="0.00" step="0.01" id="amount" value={self.new_amount.to_string()} placeholder="100.00"/>
                                          </div>
                                          <div class="form-group">
                                          <label for="desc">{"Description:"}</label>
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateDescription(t)
                                            })}
                                           type="text" class="form-control" id="name" value={self.new_description.clone()} placeholder="ex: IGA purchases"/>
                                           </div>
                                           <div class="form-group">
                                           <RadioGroup
                                                selected_type={transaction_type}
                                                on_change={on_change}
                                            />
                                           </div>
                                           <div class="form-group">
                                          <label for="tagselect">{"Choose tag:"}</label>
                                           {
                                            for self.tag_list.iter().map(|mytag|{
                                                let tagclone= mytag.clone();
                                                html!{
                                                    <>
                                                        <button type="button" onclick={
                                                        ctx.link().callback( move |_e: MouseEvent|{
                                                              Msg::SelectTag(tagclone.color.clone())
                                                           })} class={classes!("btn","btn-secondary",tagclone.color.clone(),if tagclone.color.to_string()==self.selected_tag_name.clone().to_string() {""}else{"unselected-tag "})} >
                                                        {tagclone.name.clone()}
                                                        </button>
                                                    </>
                                                }
                                            })
                                           }                                           
                                          </div>
                                          <div class="form-group">
                                          <input onchange={
                                            ctx.link().callback(move |e:Event|{
                                               let t =  e.target().unwrap()
                                               .dyn_into::<HtmlInputElement>().unwrap()
                                               .value();
                                                Msg::UpdateDatetime(t)
                                            })}
                                           type="date" class="form-control" id="date" value={self.new_date.clone()} placeholder="2023-05-20"/>
                                           </div>
                                      </form>
                                      </div>                      
                               </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{"Close"}</button>
                                <button onclick={
                                    ctx.link().callback(move |_e: MouseEvent|{
                                          Msg::CreateTransaction()})}  type="button" class="btn btn-primary" data-dismiss="modal">{"Save changes"}</button>
                              </div>
                            </div>
                          </div>
                        </div>                        
                      </div>
                      </>
                  }
              }else{html!{}}}
          </div>
        </>
        }
    }else{
        html!{
            <div>{"Loading... or nothing found"}</div>
        }
    }
    }
}
#[derive(Properties, Clone,PartialEq)]
struct RadioGroupProps {
    selected_type: TransactionType,
    on_change: Callback<TransactionType>,
}
struct RadioGroup ;
impl Component for RadioGroup {
    type Message = ();
    type Properties = RadioGroupProps;
    fn create(_ctx: &Context<Self>) -> Self {
       Self
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let selected_type = ctx.props().selected_type.clone();
        let on_change = ctx.props().on_change.clone();
        let on_change2 = ctx.props().on_change.clone();

        html! {
            <fieldset class="form-group">
                <div class="form-check">
                <input class="form-check-input"
                    type="radio"
                    id="income"
                    name="transaction-type"
                    checked={selected_type == TransactionType::Income}
                    onclick={ctx.link().callback(move |_| on_change.emit(TransactionType::Income))}
                />
                <label class="form-check-label" for="income">{"Income"}</label>
                </div>

                <div class="form-check">
                <input class="form-check-input"
                    type="radio"
                    id="expense"
                    name="transaction-type"
                    checked={selected_type == TransactionType::Expense}
                    onclick={ctx.link().callback(move |_| on_change2.emit(TransactionType::Expense))}
                />
                <label class="form-check-label" for="expense">{"Expense"}</label>
                </div>
            </fieldset>
        }
    }
}
#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeGetBudgetList, catch)]
    pub async fn get_budget_list() -> Result<JsValue, JsValue>;
}
#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeCreateTransaction, catch)]
    pub async fn create_transaction(budgetid: String,is_goal:String,transac:String) -> Result<JsValue, JsValue>;
}
#[wasm_bindgen(module = "/public/app.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeDeleteTransaction, catch)]
    pub async fn delete_transaction(monthid:String,budgetid: String,is_goal:String,index:String) -> Result<JsValue, JsValue>;
}