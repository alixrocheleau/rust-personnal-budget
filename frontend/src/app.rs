use crate::comps::navbar::Navbar;
use crate::pages::{budget::BudgetData, home::Home, month::MonthData, tags::Tags};
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Routable, PartialEq, Eq, Clone, Debug)]
pub enum Route {
    #[at("/month/:budget_id/:id")]
    MonthData { budget_id: i32, id: i32 },
    #[at("/budget/:id")]
    BudgetData { id: i32 },
    #[at("/tags")]
    Tags,
    #[not_found]
    #[at("/404")]
    NotFound,
    #[at("/")]
    Home,
}
#[derive(Clone, Debug, PartialEq)]
pub struct App;

pub fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Home /> },
        Route::Tags => html! { <Tags /> },
        Route::BudgetData { id } => html! { <BudgetData id={id}/> },
        Route::MonthData { budget_id, id } => {
            html! { <MonthData budget_id={budget_id-1} id={id}/> }
        }
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}
impl Component for App {
    type Message = ();
    type Properties = ();
    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }
    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        true
    }
    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <BrowserRouter>
                 <Navbar />
                    <div class="main">
                        <div class="content">
                            <Switch<Route> render={switch} />
                        </div>
                        <footer class="footer">
                            <div class="container">
                                {"© Alix Rocheleau 2023 sirocheleaua@gmail.com"}
                            </div>
                        </footer>
                    </div>
                </BrowserRouter>
        }
    }
}
