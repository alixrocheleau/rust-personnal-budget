use core::cmp::Ordering;

use serde::Deserialize;
use serde::Serialize;
#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Transaction {
    pub transaction_type: TransactionType,
    pub amount: f64,
    pub description: String,
    pub date: String,
    pub tags: Vec<Tag>,
}

pub fn sort_transactions(mut vec: Vec<Transaction>) -> Vec<Transaction> {
    vec.sort_by(|a, b| {
        if a.transaction_type == b.transaction_type {
            a.date.partial_cmp(&b.date).unwrap()
        } else {
            a.transaction_type.partial_cmp(&b.transaction_type).unwrap()
        }
    });
    vec
}

// month.goal_transactions.sort_by(|a, b| {
//     if a.transaction_type == b.transaction_type {
//         a.date.partial_cmp(&b.date).unwrap()
//     } else {
//         a.transaction_type.partial_cmp(&b.transaction_type).unwrap()
//     }
// });
impl PartialOrd for Transaction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.transaction_type == other.transaction_type {
            return Some(self.date.cmp(&other.date));
        }
        Some(self.transaction_type.cmp(&other.transaction_type))
    }
}

#[derive(Debug, PartialEq, Clone, Ord, PartialOrd, Eq, Deserialize, Serialize)]
pub enum TransactionType {
    Income,
    Expense,
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Month {
    pub id: i32,
    pub name: String,
    pub goal_transactions: Vec<Transaction>,
    pub transactions: Vec<Transaction>,
}

impl Month {
    pub fn get_total_goal_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for transac in self.goal_transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => totincome = totincome + transac.amount,
                TransactionType::Expense => {}
            }
        }
        return totincome;
    }
    pub fn get_total_goal_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for transac in self.goal_transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => {}
                TransactionType::Expense => totexpanse = totexpanse + transac.amount,
            }
        }
        return totexpanse;
    }
    pub fn get_total_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for transac in self.transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => totincome = totincome + transac.amount,
                TransactionType::Expense => {}
            }
        }
        return totincome;
    }
    pub fn get_total_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for transac in self.transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => {}
                TransactionType::Expense => totexpanse = totexpanse + transac.amount,
            }
        }
        return totexpanse;
    }
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Budget {
    pub id: i32,
    pub name: String,
    pub months: Vec<Month>,
}

impl Budget {
    pub fn init(&mut self) {
        let month0 = Month {
            name: String::from("January"),
            id: 0,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month1 = Month {
            name: String::from("February"),
            id: 1,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month2 = Month {
            name: String::from("March"),
            id: 2,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month3 = Month {
            name: String::from("April"),
            id: 3,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month4 = Month {
            name: String::from("May"),
            id: 4,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month5 = Month {
            name: String::from("June"),
            id: 5,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month6 = Month {
            name: String::from("July"),
            id: 6,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month7 = Month {
            name: String::from("August"),
            id: 7,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month8 = Month {
            name: String::from("September"),
            id: 8,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month9 = Month {
            name: String::from("October"),
            id: 9,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month10 = Month {
            name: String::from("November"),
            id: 10,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month11 = Month {
            name: String::from("December"),
            id: 11,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        self.months.push(month0);
        self.months.push(month1);
        self.months.push(month2);
        self.months.push(month3);
        self.months.push(month4);
        self.months.push(month5);
        self.months.push(month6);
        self.months.push(month7);
        self.months.push(month8);
        self.months.push(month9);
        self.months.push(month10);
        self.months.push(month11);
    }
    pub fn get_goal_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for month in self.months.iter() {
            totincome = totincome + month.get_total_goal_income();
        }
        return totincome;
    }
    pub fn get_goal_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for month in self.months.iter() {
            totexpanse = totexpanse + month.get_total_goal_expanse();
        }
        return totexpanse;
    }
    pub fn get_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for month in self.months.iter() {
            totincome = totincome + month.get_total_income();
        }
        return totincome;
    }
    pub fn get_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for month in self.months.iter() {
            totexpanse = totexpanse + month.get_total_expanse();
        }
        return totexpanse;
    }
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Tag {
    pub name: String,
    pub color: String,
}
