use crate::pages::tags;
use yew::prelude::*;

struct ColorItem {
    label: String,
}
pub struct ColorSelector {
    colors: Vec<ColorItem>,
}
#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub current_name: String,
    pub current_color: String,
    pub on_color_clicked: Callback<tags::Msg>,
}
pub enum Msg {
    ColorItemClick(tags::Msg),
}
impl Component for ColorSelector {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        ColorSelector {
            colors: vec![
                ColorItem {
                    label: "bg-primary".to_string(),
                },
                ColorItem {
                    label: "bg-secondary".to_string(),
                },
                ColorItem {
                    label: "bg-success".to_string(),
                },
                ColorItem {
                    label: "bg-danger".to_string(),
                },
                ColorItem {
                    label: "bg-warning".to_string(),
                },
                ColorItem {
                    label: "bg-info".to_string(),
                },
                ColorItem {
                    label: "bg-light".to_string(),
                },
                ColorItem {
                    label: "bg-dark".to_string(),
                },
            ],
        }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::ColorItemClick(msg) => {
                ctx.props().on_color_clicked.emit(msg);
            }
        }
        true
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
        <>
        <div class="color-selector d-flex align-items-center flex-fill flex-wrap">
            {
            for self.colors.iter().map(|col|{
                let color_name=col.label.clone();
                html!{
                    <div onclick={ctx.link().callback(move|_e:MouseEvent| {
                            Msg::ColorItemClick(
                                tags::Msg::UpdateColor(color_name.clone(),)
                            )})} class={classes!("color-option",col.label.clone(),if col.label==ctx.props().current_color{"colorselected"}else{""})} >
                    </div>
                    }
                }
            )
        }
        </div>
        <br/>
        <div class="d-flex align-items-center justify-content-center">
            <span class={classes!("d-flex","badge","rounded-pill",ctx.props().current_color.clone())} >{ctx.props().current_name.clone()}</span>
        </div>
        <br/>
        </>
        }
    }
}
