use crate::app::Route;
use yew::prelude::*;
use yew_router::prelude::*;

struct NavItem {
    link: Route,
    label: String,
    id: u32,
    is_active: bool,
}
pub struct Navbar {
    nav_items: Vec<NavItem>,
}
pub enum Msg {
    NavItemClick(u32),
}
impl Component for Navbar {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let path: Route = ctx.link().route().unwrap();
        Navbar {
            nav_items: vec![
                NavItem {
                    link: Route::Home,
                    label: "Budgets".to_string(),
                    id: 0,
                    is_active: path == Route::Home,
                },
                NavItem {
                    link: Route::Tags,
                    label: "Tags".to_string(),
                    id: 1,
                    is_active: path == Route::Tags,
                },
            ],
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::NavItemClick(id) => {
                match id {
                    0 => {
                        // log::info!("{}", 0);
                        self.nav_items[0].is_active = true;
                        self.nav_items[1].is_active = false;
                    }
                    1 => {
                        self.nav_items[0].is_active = false;
                        self.nav_items[1].is_active = true;
                    }
                    _ => {
                        log::info!("{}", "Nav nothing");
                    }
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let navigator = ctx.link().navigator().unwrap();
        let onback = Callback::from(move |_| navigator.back());
        html! {
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                // <a class="navbar-brand ml-4" href="/">{"Personal Budget"}</a>
                <a class="navbar-brand ml-4 nav-item nav-link" href="#" onclick={onback}>{"Back"}</a>
                <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        { for self.nav_items.iter().map(|nav_item| {
                          let index=nav_item.id.clone();
                            let onclick = ctx.link().callback(move |_e: MouseEvent|{
                              Msg::NavItemClick(index)
                           });
                            html! {
                                <li
                                    onclick={onclick}
                                    key={ nav_item.id }
                                    class={ classes!("nav-item", if nav_item.is_active==true{"active"} else {""}) }
                                >
                                     <Link<Route> classes={"nav-link"} to={nav_item.link.clone()}>{nav_item.label.clone()}</Link<Route>>
                                </li>
                            }
                        })
                      }
                    </ul>
                </div>
            </nav>
        }
    }
}
// <li class="nav-item dropdown">
//                 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                   {"Services"}
//                 </a>
//                 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
//                   <a class="dropdown-item" href="#">{"Budgets"}</a>
//                   <a class="dropdown-item" href="#">{"Tags"}</a>
//                   <div class="dropdown-divider"></div>
//                   <a class="dropdown-item" href="#">{"More Services"}</a>
//                 </div>
//               </li>
//               <li class="nav-item">
//                 <a class="nav-link" href="#">{"Contact"}</a>
//               </li>
