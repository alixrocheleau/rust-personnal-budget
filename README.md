# rust-personal-budget

## Getting started

Simple app to create budgets, tags, add transactions to months' goal/real expenses.

Make sure to create a few tags first and probably at least 1 basic default one with the first blue color :)

Enjoy.

## Rust / WASM

Built with Rust, Yew, Tauri in WASM.

Rust for the code, Yew is for the frontend and Tauri is for the Backend.

## Name

My Rust-Personal-Budget :)

## Description

Personal budget app to figure out your expenses and income for the month(s).

## Installation

Download the released installer.

## Authors and acknowledgment

By Alix Rocheleau, 2023

## License

Open source MIT

## Project status

I don't intend to add more content unless my girlfriend needs more options

## Sneak-peek?

Check the img folder for an overview