// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use serde::Deserialize;
use serde::Serialize;
use std::collections::HashMap;
use std::env;
use std::io::Seek;
use std::io::SeekFrom;

use std::fs::{File, OpenOptions};
use std::io::{Read, Write};

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            delete_tag,
            get_budget_list,
            hello,
            create_tag,
            create_budget,
            delete_budget,
            create_transaction,
            delete_transaction,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
#[tauri::command(rename_all = "snake_case")]
fn delete_transaction(
    monthid: String,
    budgetid: String,
    is_goal: String,
    index: String,
) -> Result<String, String> {
    let month_id: i32 = match serde_json::from_str(&monthid) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let budget_id: i32 = match serde_json::from_str(&budgetid) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let is_a_goal: bool = match serde_json::from_str(&is_goal) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let transaction_id: i32 = match serde_json::from_str(&index) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(&full_file_path)
        .map_err(|err| err.to_string())?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|err| err.to_string())?;

    let mut json_data: serde_json::Value =
        serde_json::from_str(&contents).map_err(|err| err.to_string())?;
    // Deserialize the budgetlist property into a Vec<Budget>
    let budget_list_json: Vec<String> = match json_data.get("budgetlist") {
        Some(budgetlist) => {
            serde_json::from_value(budgetlist.clone()).map_err(|err| err.to_string())?
        }
        None => return Err("budgetlist not found in JSON".to_string()),
    };

    let mut budget_list: Vec<Budget> = budget_list_json
        .into_iter()
        .map(|budget_str| serde_json::from_str(&budget_str).map_err(|err| err.to_string()))
        .collect::<Result<Vec<Budget>, String>>()?;
    // Find the position of the budget with matching id
    let budget_position = budget_list.iter().position(|budget| budget.id == budget_id);

    if is_a_goal {
        budget_list[budget_position.unwrap()].months[month_id as usize]
            .goal_transactions
            .remove(transaction_id as usize);
    } else {
        budget_list[budget_position.unwrap()].months[month_id as usize]
            .transactions
            .remove(transaction_id as usize);
    }

    // Serialize the updated budget_list back into JSON strings
    let updated_budgetlist: Vec<String> = budget_list
        .iter()
        .map(|budget| serde_json::to_string(budget).map_err(|err| err.to_string()))
        .collect::<Result<Vec<String>, String>>()?;

    // Replace the budgetlist property in json_data with the updated_budgetlist
    json_data["budgetlist"] = serde_json::Value::from(updated_budgetlist);

    // Write the updated json_data back to the file
    let updated_json = serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;

    file.set_len(0).map_err(|err| err.to_string())?;
    file.seek(SeekFrom::Start(0))
        .map_err(|err| err.to_string())?;
    file.write_all(updated_json.as_bytes())
        .map_err(|err| err.to_string())?;

    Ok("Transaction deleted successfully".to_string())
}

#[tauri::command(rename_all = "snake_case")]
fn create_transaction(
    budgetid: String,
    is_goal: String,
    transac: String,
) -> Result<String, String> {
    let budget_id: i32 = match serde_json::from_str(&budgetid) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let is_a_goal: bool = match serde_json::from_str(&is_goal) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let new_transac: Transaction = match serde_json::from_str(&transac) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(&full_file_path)
        .map_err(|err| err.to_string())?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|err| err.to_string())?;

    let mut json_data: serde_json::Value =
        serde_json::from_str(&contents).map_err(|err| err.to_string())?;
    // Deserialize the budgetlist property into a Vec<Budget>
    let budget_list_json: Vec<String> = match json_data.get("budgetlist") {
        Some(budgetlist) => {
            serde_json::from_value(budgetlist.clone()).map_err(|err| err.to_string())?
        }
        None => return Err("budgetlist not found in JSON".to_string()),
    };

    let mut budget_list: Vec<Budget> = budget_list_json
        .into_iter()
        .map(|budget_str| serde_json::from_str(&budget_str).map_err(|err| err.to_string()))
        .collect::<Result<Vec<Budget>, String>>()?;
    // Find the position of the budget with matching id
    let budget_position = budget_list.iter().position(|budget| budget.id == budget_id);

    let month_index = new_transac
        .date
        .split('-')
        .nth(1)
        .and_then(|s| s.parse::<usize>().ok());
    if is_a_goal {
        budget_list[budget_position.unwrap()].months[month_index.unwrap() - 1]
            .goal_transactions
            .push(new_transac.clone());
    } else {
        budget_list[budget_position.unwrap()].months[month_index.unwrap() - 1]
            .transactions
            .push(new_transac.clone());
    }

    // Serialize the updated budget_list back into JSON strings
    let updated_budgetlist: Vec<String> = budget_list
        .iter()
        .map(|budget| serde_json::to_string(budget).map_err(|err| err.to_string()))
        .collect::<Result<Vec<String>, String>>()?;

    // Replace the budgetlist property in json_data with the updated_budgetlist
    json_data["budgetlist"] = serde_json::Value::from(updated_budgetlist);

    // Write the updated json_data back to the file
    let updated_json = serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;

    file.set_len(0).map_err(|err| err.to_string())?;
    file.seek(SeekFrom::Start(0))
        .map_err(|err| err.to_string())?;
    file.write_all(updated_json.as_bytes())
        .map_err(|err| err.to_string())?;

    Ok("Transaction added successfully".to_string())
}

#[tauri::command(rename_all = "snake_case")]
fn delete_budget(index: String) -> Result<String, String> {
    let index: usize = match serde_json::from_str(&index) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);

    if !full_file_path.exists() {
        return Ok("File does not exist".to_string());
    }

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(&full_file_path)
        .map_err(|err| err.to_string())?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|err| err.to_string())?;

    let mut json_data: serde_json::Value =
        serde_json::from_str(&contents).map_err(|err| err.to_string())?;

    let tag_list = json_data
        .get_mut("budgetlist")
        .and_then(|value| value.as_array_mut())
        .ok_or("Invalid JSON")?;

    if index >= tag_list.len() {
        return Err("Invalid index".to_string());
    }

    tag_list.remove(index);

    let updated_json = serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;
    file.set_len(0).map_err(|err| err.to_string())?;
    file.seek(SeekFrom::Start(0))
        .map_err(|err| err.to_string())?;
    file.write_all(updated_json.as_bytes())
        .map_err(|err| err.to_string())?;

    Ok("Success".to_string())
}

#[tauri::command(rename_all = "snake_case")]
fn create_budget(budget: String) -> Result<String, String> {
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);

    if !full_file_path.exists() {
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(&full_file_path)
            .map_err(|err| err.to_string())?;
        let empty_json = serde_json::to_string_pretty(&serde_json::json!({
            "budgetlist": [budget],
            "taglist": []  // Add the tag to the taglist
        }))
        .map_err(|err| err.to_string())?;
        file.write_all(empty_json.as_bytes())
            .map_err(|err| err.to_string())?;
    } else {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&full_file_path)
            .map_err(|err| err.to_string())?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .map_err(|err| err.to_string())?;

        let mut json_data: serde_json::Value =
            serde_json::from_str(&contents).map_err(|err| err.to_string())?;
        let budget_list = json_data.get_mut("budgetlist").ok_or("Invalid JSON")?;
        budget_list
            .as_array_mut()
            .ok_or("Invalid JSON")?
            .push(serde_json::to_value(budget).map_err(|err| err.to_string())?);

        let updated_json =
            serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;
        file.set_len(0).map_err(|err| err.to_string())?;
        file.seek(SeekFrom::Start(0))
            .map_err(|err| err.to_string())?;
        file.write_all(updated_json.as_bytes())
            .map_err(|err| err.to_string())?;
    }

    Ok("Budget added successfully".to_string())
}
#[tauri::command(rename_all = "snake_case")]
fn delete_tag(index: String) -> Result<String, String> {
    let index: usize = match serde_json::from_str(&index) {
        Ok(idx) => idx,
        Err(err) => return Err(err.to_string()),
    };

    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);

    if !full_file_path.exists() {
        return Ok("File does not exist".to_string());
    }

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(&full_file_path)
        .map_err(|err| err.to_string())?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|err| err.to_string())?;

    let mut json_data: serde_json::Value =
        serde_json::from_str(&contents).map_err(|err| err.to_string())?;

    let tag_list = json_data
        .get_mut("taglist")
        .and_then(|value| value.as_array_mut())
        .ok_or("Invalid JSON")?;

    if index >= tag_list.len() {
        return Err("Invalid index".to_string());
    }

    tag_list.remove(index);

    let updated_json = serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;
    file.set_len(0).map_err(|err| err.to_string())?;
    file.seek(SeekFrom::Start(0))
        .map_err(|err| err.to_string())?;
    file.write_all(updated_json.as_bytes())
        .map_err(|err| err.to_string())?;

    Ok("Success".to_string())
}

#[tauri::command(rename_all = "snake_case")]
fn create_tag(tag_str: String) -> Result<String, String> {
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);

    if !full_file_path.exists() {
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(&full_file_path)
            .map_err(|err| err.to_string())?;
        let empty_json = serde_json::to_string_pretty(&serde_json::json!({
            "budgetlist": [],
            "taglist": [tag_str]  // Add the tag to the taglist
        }))
        .map_err(|err| err.to_string())?;
        file.write_all(empty_json.as_bytes())
            .map_err(|err| err.to_string())?;
    } else {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&full_file_path)
            .map_err(|err| err.to_string())?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .map_err(|err| err.to_string())?;

        let mut json_data: serde_json::Value =
            serde_json::from_str(&contents).map_err(|err| err.to_string())?;
        let tag_list = json_data.get_mut("taglist").ok_or("Invalid JSON")?;
        tag_list
            .as_array_mut()
            .ok_or("Invalid JSON")?
            .push(serde_json::to_value(tag_str).map_err(|err| err.to_string())?);

        let updated_json =
            serde_json::to_string_pretty(&json_data).map_err(|err| err.to_string())?;
        file.set_len(0).map_err(|err| err.to_string())?;
        file.seek(SeekFrom::Start(0))
            .map_err(|err| err.to_string())?;
        file.write_all(updated_json.as_bytes())
            .map_err(|err| err.to_string())?;
    }

    Ok("Tag added successfully".to_string())
}

#[tauri::command(rename_all = "snake_case")]
fn get_budget_list() -> Result<String, String> {
    let res = env::current_dir();
    let base_dir = match res {
        Ok(dir) => dir,
        Err(err) => return Err(err.to_string()),
    };
    let file_path = "budgetssavedata.json".to_string();

    let full_file_path = base_dir.join(&file_path);

    if !full_file_path.exists() {
        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(&full_file_path)
            .map_err(|err| err.to_string())?;
        let empty_json = serde_json::to_string_pretty(&serde_json::json!({
            "budgetlist": [],
            "taglist": []
        }))
        .map_err(|err| err.to_string())?;
        file.write_all(empty_json.as_bytes())
            .map_err(|err| err.to_string())?;
    }

    let mut file = File::open(full_file_path).map_err(|err| err.to_string())?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|err| err.to_string())?;

    // let json_data: serde_json::Value =
    //     serde_json::from_str(&contents).map_err(|err| err.to_string())?;
    // let budget_list: Vec<Budget> =
    //     serde_json::from_value(json_data["budgetlist"].clone()).map_err(|err| err.to_string())?;
    // let tag_list: Vec<Tag> =
    //     serde_json::from_value(json_data["taglist"].clone()).map_err(|err| err.to_string())?;

    Ok(contents)
}

#[tauri::command(rename_all = "snake_case")]
fn hello(name: &str) -> Result<String, String> {
    // This is a very simplistic example but it shows how to return a Result
    // and use it in the front-end.
    if name.contains(' ') {
        Err("Name should not contain spaces".to_string())
    } else {
        let res = env::current_dir();
        let base_dir = match res {
            Ok(dir) => dir,
            Err(err) => return Err(err.to_string()),
        };
        let file_path = "budgetssavedata.json";

        let full_file_path = base_dir.join(&file_path);
        Ok(format!(
            "{:?}",
            full_file_path.into_os_string().into_string().clone()
        ))
    }
}

use core::cmp::Ordering;

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Transaction {
    pub transaction_type: TransactionType,
    pub amount: f64,
    pub description: String,
    pub date: String,
    pub tags: Vec<Tag>,
}

pub fn sort_transactions(mut vec: Vec<Transaction>) -> Vec<Transaction> {
    vec.sort_by(|a, b| {
        if a.transaction_type == b.transaction_type {
            a.date.partial_cmp(&b.date).unwrap()
        } else {
            a.transaction_type.partial_cmp(&b.transaction_type).unwrap()
        }
    });
    vec
}

// month.goal_transactions.sort_by(|a, b| {
//     if a.transaction_type == b.transaction_type {
//         a.date.partial_cmp(&b.date).unwrap()
//     } else {
//         a.transaction_type.partial_cmp(&b.transaction_type).unwrap()
//     }
// });
impl PartialOrd for Transaction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.transaction_type == other.transaction_type {
            return Some(self.date.cmp(&other.date));
        }
        Some(self.transaction_type.cmp(&other.transaction_type))
    }
}

#[derive(Debug, PartialEq, Clone, Ord, PartialOrd, Eq, Deserialize, Serialize)]
pub enum TransactionType {
    Income,
    Expense,
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Month {
    pub id: i32,
    pub name: String,
    pub goal_transactions: Vec<Transaction>,
    pub transactions: Vec<Transaction>,
}

impl Month {
    pub fn get_total_goal_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for transac in self.goal_transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => totincome = totincome + transac.amount,
                TransactionType::Expense => {}
            }
        }
        return totincome;
    }
    pub fn get_total_goal_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for transac in self.goal_transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => {}
                TransactionType::Expense => totexpanse = totexpanse + transac.amount,
            }
        }
        return totexpanse;
    }
    pub fn get_total_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for transac in self.transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => totincome = totincome + transac.amount,
                TransactionType::Expense => {}
            }
        }
        return totincome;
    }
    pub fn get_total_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for transac in self.transactions.iter() {
            match &transac.transaction_type {
                TransactionType::Income => {}
                TransactionType::Expense => totexpanse = totexpanse + transac.amount,
            }
        }
        return totexpanse;
    }
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Budget {
    pub id: i32,
    pub name: String,
    pub months: Vec<Month>,
}

impl Budget {
    pub fn init(&mut self) {
        let month0 = Month {
            name: String::from("January"),
            id: 0,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month1 = Month {
            name: String::from("February"),
            id: 1,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month2 = Month {
            name: String::from("March"),
            id: 2,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month3 = Month {
            name: String::from("April"),
            id: 3,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month4 = Month {
            name: String::from("May"),
            id: 4,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month5 = Month {
            name: String::from("June"),
            id: 5,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month6 = Month {
            name: String::from("July"),
            id: 6,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month7 = Month {
            name: String::from("August"),
            id: 7,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month8 = Month {
            name: String::from("September"),
            id: 8,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month9 = Month {
            name: String::from("October"),
            id: 9,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month10 = Month {
            name: String::from("November"),
            id: 10,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        let month11 = Month {
            name: String::from("December"),
            id: 11,
            transactions: Vec::new(),
            goal_transactions: Vec::new(),
        };
        self.months.push(month0);
        self.months.push(month1);
        self.months.push(month2);
        self.months.push(month3);
        self.months.push(month4);
        self.months.push(month5);
        self.months.push(month6);
        self.months.push(month7);
        self.months.push(month8);
        self.months.push(month9);
        self.months.push(month10);
        self.months.push(month11);
    }
    pub fn get_goal_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for month in self.months.iter() {
            totincome = totincome + month.get_total_goal_income();
        }
        return totincome;
    }
    pub fn get_goal_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for month in self.months.iter() {
            totexpanse = totexpanse + month.get_total_goal_expanse();
        }
        return totexpanse;
    }
    pub fn get_income(&self) -> f64 {
        let mut totincome: f64 = 0.0;
        for month in self.months.iter() {
            totincome = totincome + month.get_total_income();
        }
        return totincome;
    }
    pub fn get_expanse(&self) -> f64 {
        let mut totexpanse: f64 = 0.0;
        for month in self.months.iter() {
            totexpanse = totexpanse + month.get_total_expanse();
        }
        return totexpanse;
    }
}

#[derive(Debug, PartialEq, Clone, Deserialize, Serialize)]
pub struct Tag {
    pub name: String,
    pub color: String,
}
